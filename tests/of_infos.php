<?php
require_once '../lib/of_infos.php';

$of_infos = new of_infos( array('lala', 'lulu', 'lölö') );
print ( $of_infos->as_string() . "\n");
print_r( $of_infos->as_array() );

$of_infos = new of_infos( 'dada / dudu/ dödö' );
print ( $of_infos->as_string() . "\n");
print_r( $of_infos->as_array() );
