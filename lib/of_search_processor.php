<?php

class of_search_processor
{

    private $rcmail;

    private $openflags;

    private $name;

    private $message_priv_labels;

    public function __construct ($rcmail, $openflags)
    {
        $this->rcmail = $rcmail;
        $this->openflags = $openflags;
        $this->name = $openflags->name;
        
        $this->openflags->add_button(
                array(
                        'type' => 'link',
                        'label' => 'xsearch',
                        'command' => 'plugin.openflags-get_searchformdata',
                        'class' => 'button search xsearch',
                        'classact' => 'button search xsearch',
                        'width' => 32,
                        'height' => 32,
                        'title' => 'xsearchtitle',
                        'domain' => $this->ID
                ), 'toolbar');
    }

    public function get_searchformdata ()
    {
        $flags = $this->openflags->get_flags(
                array(
                        'info',
                        'person'
                ));
        $this->rcmail->output->command('plugin.openflags-show_search_dialog', 
                array(
                        'type' => 'search',
                        'value' => $flags
                ));
    }

    public function search ()
    {
        $imap_charset = RCUBE_CHARSET;
        $sort_column = rcmail_sort_column();
        $sort_column = "";
        
        $str = rcube_utils::get_input_value('_xs_q', rcube_utils::INPUT_GET, 
                true);
        $mbox = rcube_utils::get_input_value('_mbox', rcube_utils::INPUT_GET, 
                true);
        $filter = rcube_utils::get_input_value('_filter', 
                rcube_utils::INPUT_GET, true);
        $headers = rcube_utils::get_input_value('_headers', 
                rcube_utils::INPUT_GET, true);
        $infos = rcube_utils::get_input_value('info', rcube_utils::INPUT_GET, 
                true);
        $persons = rcube_utils::get_input_value('person', 
                rcube_utils::INPUT_GET, true);
        $scope = rcube_utils::get_input_value('_xs_scope', 
                rcube_utils::INPUT_GET, true);
        
        $search_request = md5($mbox . $filter . $str);
        
        $query = array();
        
        $flags = array();
        $subject = array();
        
        $boxes = array();
        
        switch ($scope) {
            case 'recurse':
                $boxes = $this->rcmail->storage->list_folders_subscribed($mbox);
                if( $mbox != 'INBOX' && in_array( 'INBOX', $boxes )){
                    $boxes = array_diff($boxes, array("INBOX"));
                }
                break;
            case 'all':
                $boxes = $this->rcmail->storage->list_folders_subscribed();
                break;
            default:
                $scope = false;
                $boxes[] = $mbox;
                break;
        }
        
        if ($str) {
            $subject['subjects'] = explode(' ', $str);
            $subject['type'] = rcube_utils::get_input_value('_xs_q_type', 
                    rcube_utils::INPUT_GET, true);
            $query['subject'] = $subject;
        }
        
        if ($infos) {
            $flags['infos'] = array();
            if (is_array($infos)) {
                foreach ($infos as $tag) {
                    $flags['infos'][] = $tag;
                }
            } else {
                $flags['infos'][] = $infos;
            }
        }
        if ($persons) {
            require_once 'of_person.php';
            $flags['persons'] = array();
            if (is_array($persons)) {
                foreach ($persons as $tag) {
                    $flags['persons'][] = of_person::create_flag($tag);
                }
            } else {
                $flags['persons'][] = of_person::create_flag($persons);
            }
        }
        $query['flags'] = $flags;
        
        /*$query['searchfields'] = array(
                "HEADER SUBJECT",
                "HEADER FROM",
                "HEADER TO",
                "HEADER CC",
                "HEADER BCC",
                "BODY"
        );*/
        $query['searchfields'] = array(
         "TEXT"
        );
        
        $search_str = $this->generate_query($query, $imap_charset);
        $result_h = array();
        $last = 0;
        
        $search_result_boxes = array();
        $sr_id = 0;
        foreach ($boxes as $box) {

            // execute IMAP search
            if ($search_str) {
                list($mbox, $respond) = $this->rcmail->storage->search($box, $search_str, $imap_charset
                        ,$sort_column);
            }
            
            if( $respond ){
                rcube::write_log( $this->name, "respond: " . $respond);
            }
            
            // Get the headers
            $headers = $this->rcmail->storage->list_messages($box, 1, 
                    $sort_column, rcmail_sort_order());
            foreach ($headers as $header) {
                $sr_id ++;
                $header->mbox = $box;
                $result_h[] = $header;
                if ($scope) {
                    $search_result_boxes[$sr_id] = array(
                            'uid' => $header->uid,
                            'mbox' => $box
                    );
                    $header->uid = $sr_id;
                }
                if( $sr_id >= 100 ){
                    break;
                }
            }
            $anz = $sr_id - $last;
            rcube::write_log($this->name, "Search in $box - found: $anz  / Total: $sr_id");
            $last = $sr_id;
            if( $sr_id >= 100 ){
                rcube::write_log($this->name, "Zu viele Suchergebnisse");
                break;
            }
        }
        
        // Make sure we got the headers
        
        if (! empty($result_h)) {
            // rcube::write_log($this->name, "*** Set Message List ***");
            if ($scope) {
                rcube::write_log($this->name, "*** Scope set ***");
                $this->rcmail->output->set_env('all_folder_search_active', true);
                $_SESSION['all_folder_search']['uid_mboxes'] = $search_result_boxes;
                $this->rcmail->output->set_env('all_folder_search_uid_mboxes', 
                        $search_result_boxes);
                $this->rcmail->output->set_env('search_request', 
                        $search_str ? 'allfoldersearch' : '');
            } else {
                $this->rcmail->output->set_env('search_request', 
                        $search_str ? $search_request : '');
            }
            rcmail_js_message_list($result_h);
            if ($search_str) {
                if( $sr_id < 100 ){
                $this->rcmail->output->show_message('searchsuccessful', 
                        'confirmation', 
                        array(
                                'nr' => $sr_id
                        ));
                }else{
                    $this->rcmail->output->show_message('openflags.ofs_too_much_results',
                            'warning',
                            array(
                                    'nr' => $sr_id
                            ));                   
                }
                // remember last HIGHESTMODSEQ value (if supported)
                // we need it for flag updates in check-recent
                $data = $this->rcmail->storage->folder_data($mbox_name);
                if (! empty($data['HIGHESTMODSEQ'])) {
                    $_SESSION['list_mod_seq'] = $data['HIGHESTMODSEQ'];
                }
            }
        }         
        else 
            // handle IMAP errors (e.g. #1486905)
            if ($err_code = $this->rcmail->storage->get_error_code()) {
                $this->rcmail->display_server_error();
            } else {
                $this->rcmail->output->show_message('searchnomatch', 'notice');
            }
        
        // update message count display
        $this->rcmail->output->set_env('messagecount', $sr_id);
        $this->rcmail->output->set_env('pagecount', 1);
        $this->rcmail->output->set_env('pagesize', $sr_id);
        /*$this->rcmail->output->set_env('pagecount', 
                ceil($sr_id / $this->rcmail->storage->get_pagesize()));*/
        $this->rcmail->output->set_env('exists', 
                $this->rcmail->storage->count($mbox_name, 'EXISTS'));
        $this->rcmail->output->command('set_rowcount', 
                rcmail_get_messagecount_text($sr_id, 1), $mbox);
        $this->rcmail->output->send();
    }

    function generate_query ($query, $charset)
    {
        $querystring = "CHARSET $charset " ;
        foreach ($query as $section => $values) {
            switch ($section) {
                case 'flags':
                    foreach ($values as $flagtype => $flags) {
                        $querystring .= str_repeat(' OR', sizeof($flags) - 1) .
                                 ' ';
                        $querystring .= 'KEYWORD ' . implode(' KEYWORD ', 
                                $flags);
                    }
                    break;
                case 'subject':
                    $subjects = $values['subjects'];
                    $searchfields = $query['searchfields'];
                    
                    if ($values['type'] == 'or') {
                        $querystring .= str_repeat(' OR ', 
                                sizeof($subjects) - 1);
                    }
                    foreach ($subjects as $subject) {
                        $length = mb_strlen( $subject, '8bit');
                        $querystring .= str_repeat(' OR ', 
                                sizeof($searchfields) - 1);
                        $querystring .= implode(" {".$length."}\n$subject ", $searchfields) .
                                 " {".$length."}\n$subject ";
                    }
                    break;
            }
        }
        //rcube::write_log($this->name, $querystring, true);
        return $querystring;
    }

    function redirect ($args)
    {
        // rcube::write_log($this->name, "redirect");
        $search = rcube_utils::get_input_value('_search', RCUBE_INPUT_GET);
        if (! isset($search))
            $search = rcube_utils::get_input_value('_search', RCUBE_INPUT_POST);
        
        $uid = rcube_utils::get_input_value('_uid', RCUBE_INPUT_GET);
        $mbox = rcube_utils::get_input_value('_mbox', RCUBE_INPUT_GET);
        $page = rcube_utils::get_input_value('_page', RCUBE_INPUT_GET);
        $sort = rcube_utils::get_input_value('_sort', RCUBE_INPUT_GET);
        $lock = rcube_utils::get_input_value('_lock', RCUBE_INPUT_GET);
        $framed = rcube_utils::get_input_value('_framed', RCUBE_INPUT_GET);
        
        
        if ($search == 'allfoldersearch') {
            // rcube::write_log($this->name, "allfoldersearch, redirect");
            if (($args['action'] == 'show' || $args['action'] == 'preview') &&
                     ! empty($uid)) {
                rcube::write_log('openflags', 'redirect');
                $uid = $_SESSION['all_folder_search']['uid_mboxes'][$uid]['uid'];
                $this->rcmail->output->redirect(
                        array(
                                '_unlock' => true,
                                '_task' => 'mail',
                                '_action' => $args['action'],
                                '_mbox' => $mbox,
                                '_uid' => $uid,
                                '_framed' => $framed
                        ));
            }
        }
    }
}
