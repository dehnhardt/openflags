<?php
class of_settings_processor {
	private $rcmail;
	private $openflags;
	private $name;

	public function __construct($rcmail, $openflags){
		$this->rcmail = $rcmail;
		$this->openflags = $openflags;
		$this->name = $openflags->name;
	}

	function preferences_list($args){
    	global $CURR_SECTION;
		
    	if ($args['section'] == 'folders'  && $this->openflags->of_parts['rsm'] ){
			
			// load folders list when needed
			if ($CURR_SECTION)
				$select = $this->rcmail->folder_selector(array (
						'noselection' => '---',
						'realnames' => true,
						'maxlength' => 30,
						'folder_filter' => 'mail',
						'folder_rights' => 'w' 
				));
			else
				$select = new html_select();
			
			$args['blocks']['main']['options'][$this->openflags->rsm_folder_name] = array (
					'title' => $this->openflags->gettext('rsm'),
					'content' => $select->show($this->rcmail->config->get($this->openflags->rsm_folder_name), array (
							'name' => "_rsm_mbox" 
					)) 
			);
			
		}
		
		return $args;
	}
	
	/**
	 * Hook to save plugin-specific user settings
	 */
	function preferences_save($args)
	{
		if ($args['section'] == 'folders' && $this->openflags->of_parts['rsm']) {
			$args['prefs'][$this->openflags->rsm_folder_name] = rcube_utils::get_input_value('_rsm_mbox', rcube_utils::INPUT_POST);
			//rcube::write_log( $this->name , "FolderName: " . $this->openflags->rsm_folder_name );
			return $args;
		}
	}
	
}
