<?php

class of_infos
{

    private $infos = false;

    public function __construct ($infos = null)
    {
        $this->set_infos($infos);
    }

    function set_infos ($infos)
    {
        if ($infos) {
            if (is_array($infos)) {
                $this->infos = $infos;
            } else {
                $this->infos = preg_split("/ ?\/ ?/", $infos);
            }
        } else {
            $this->infos = false;
        }
    }
    
    function as_array(){
        return $this->infos;
    }
    
    function as_string(){
        return implode(" / ", $this->infos);
    }
}