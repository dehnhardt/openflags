<?php

class of_compose_processor
{

    private $rcmail;

    private $openflags;

    private $name;

    private $message_priv_labels;

    public function __construct ($rcmail, $openflags)
    {
        $this->rcmail = $rcmail;
        $this->openflags = $openflags;
        $this->name = $openflags->name;
        $this->rcmail->output->set_env($this->openflags->rsm_folder_name, $this->rcmail->config->get($this->openflags->rsm_folder_name));
    }

    function message_outgoing_headers ($param)
    {
        rcube::write_log($this->name, 
                "message_outgoing_headers: " . print_r($param, true));
        $flags = $param['flags'];
        if ($rsm = rcube_utils::get_input_value("flags-openflags_editrsm", 
                rcube_utils::INPUT_POST)) {
            require_once 'of_resubmission.php';
            $of_resubmission = new of_resubmission($rsm);
            $flags[] = $of_resubmission->as_flag();
        }
        if ($infos = rcube_utils::get_input_value("flags-openflags_editinfo", 
                rcube_utils::INPUT_POST)) {
            require_once 'of_infos.php';
            $of_infos = new of_infos($infos);
            $flags = array_merge($of_infos->as_array(), $flags);
        }
        if ($persons = rcube_utils::get_input_value("flags-openflags_editperson", 
                rcube_utils::INPUT_POST)) {
            require_once 'of_person.php';
            $of_person = new of_person($persons);
            $flags = array_merge($of_person->as_flag_array(), $flags);
        }
        $param['flags'] = $flags;
        return $param;
    }

    function render_page ($param)
    {
        // rcube::write_log($this->name, "render_page: " . print_r($param, true));
        $this->message_priv_labels = array();
        if ($this->openflags->of_parts['rsm']) {
            $this->message_priv_labels['rsm'][] = '';
        }
        if ($this->openflags->of_parts['info']) {
            $this->message_priv_labels['info'] = '';
        }
        if ($this->openflags->of_parts['person']) {
            $this->message_priv_labels['person'] = '';
        }
        foreach ($this->message_priv_labels as $l => $k) {
            $val = '';
            $n = 0;
            if (is_array($k)) {
                foreach ($k as $d) {
                    if ($n > 0)
                        $val .= ' / ';
                    $val .= $d;
                    $n ++;
                }
            }
            $html .= '<tr><td>' . $this->openflags->gettext($l) . ':</td>';
            $html .= '<td><input type="text" value="" readonly="readonly" name="flags-openflags_edit' .
                     $l . '" id="openflags_edit' . $l . '">' . $val . '</input>';
            $html .= '<div class="openflags_dialog" id="openflags_dialog' . $l .
                     '" title = "' . $this->openflags->gettext($l) . '"></div>';
            $html .= '</td><td>';
            $html .= '<img class="openflags_edit" src="' .
                     $this->openflags->get_urlbase() .
                     'media/Edit.png" width="16px" height="16px" ' .
                     ' onclick="compose_flag(\'openflags_dialog' . $l . '\', \'' .
                     $l . '\', \'' . false . '\')" ' . ' alt="' .
                     $this->openflags->gettext('edit') . '"/>';
            $html .= '</td></tr>';
        }
        $param['content'] = preg_replace('/(_subject.*\n*.*<\/tr>)/i', 
                '\\1' . "\n$html", $param['content']);
        return $param;
    }
}
