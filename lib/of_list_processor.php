<?php
class of_list_processor {
	private $rcmail;
	private $openflags;
	private $name;

	public function __construct($rcmail, $openflags){
		$this->rcmail = $rcmail;
		$this->openflags = $openflags;
		$this->name = $openflags->name;
		
		$this->openflags->include_script('menu.js');
		
	}

	public function messages_list($args){
		$messages = $args['messages'];
		$i = 1;
		if (is_array($messages)){
			foreach ( $messages as $message ){
				if ($message->flags){
					$message->list_cols['openflags'] = $this->analyze_open_flags($message);
				}
			}
		}
		return $args;
	}

	private function analyze_open_flags($message){
		$flag_structure = array (
				'info' => array (),
				'person' => array (),
				'rsm' => array () 
		);
		foreach ( $message->flags as $flag => $count ){
			if (preg_match('/^\$?label|SEEN|DELETED|FLAGGED|RECENT|ANSWERED|FORWARDED/', $flag)){
			}else if (preg_match('/^\@.*/', $flag)){
				if (! $this->openflags->of_parts['person']){
					continue;
				}
				$flag_structure['person'][] = str_replace('_', ' ', substr($flag, 1));
			}else if (preg_match('/^RSM:/', $flag)){
				if (! $this->openflags->of_parts['rsm']){
					continue;
				}
				$rsm = substr($flag, 4);
				$date = DateTime::createFromFormat('Y-m-d', $rsm);
				;
				$flag_structure['rsm'][0] = $date->format('d.m.Y');
			}else if ($this->openflags->of_parts['info']){
				$flag_structure['info'][] = $flag;
			}
		}
		$html = '';
		$html .= '<div id="openflags' . $message->uid . '">';
		$html .= $this->create_message_list_html($flag_structure, 'info', $message->uid);
		$html .= $this->create_message_list_html($flag_structure, 'person', $message->uid);
		$html .= $this->create_message_list_html($flag_structure, 'rsm', $message->uid);
		$html .= '</div>';
		return $html;
	}

	private function create_message_list_html($flag_structure, $index, $id){
		$cFlags = count($flag_structure[$index]);
		$html = '<div class="td_openflags">';
		if ($cFlags > 0){
			$inner_html = '<div class="openflags_popup"';
			$inner_html .= ' id="openflag_details_' . $index . $id . '">';
			$inner_html .= $this->openflags->gettext($index);
			$html .= '<img width="16" height="16" class="of_show_details" id="img_' . $index . $id .  '" src="' . $this->openflags->get_urlbase() . '/media/';
			switch ($index) {
				case 'info' :
 					$html .='Info.png"';
					break;
				case 'person' :
					$html .= 'Group.png"';
					break;
				case 'rsm' :
					$html .= 'Calendar.png"';
					break;
			}
			//$html .= ' onMouseOver="openflags_visible( \'#openflag_details_' . $index . $id . '\', this, true )"';
			//$html .= ' onMouseOut="openflags_visible( \'#openflag_details_' . $index . $id . '\', this, false )"';
			$html .= '/>';
			$inner_html = '<ul class="openflags_popup" id="menu_' . $index . $id . '">';
			foreach ( $flag_structure[$index] as $flag ){
				$inner_html .= '<li>' . $flag . '</li>';
			}
			$inner_html .= '</ul></div>';
		}else{
			$html .= "&nbsp;";
		}
		$html .= '</div>';
		$html .= $inner_html;
		return $html;
	}
	
	function startup( $args ){
		rcube::write_log($this->name, "startup");
		$cols = $this->rcmail->config->get('list_cols');
		if (array_search('openflags', $cols) === false){
			$pos = array_search('subject', $cols);
			if ($pos >= 0){
				array_splice($cols, $pos + 1, 0, 'openflags');
			}else{
				$cols[] = 'openflags';
			}
		}
		$this->rcmail->config->set('list_cols', $cols);
		return $args;
	}
	
}
