<?php

class of_person
{

    private $persons = false;

    static function create_flag ($person)
    {
    	return '@'. str_replace(' ', '_', $person);
    }
    
    static function create_person ($flag)
    {
        return str_replace( '_', ' ', substr($flag, 1));
    }
    
    public function __construct ($persons = null)
    {
        $this->set_infos($persons);
    }

    function set_infos ($persons)
    {
        if ($persons) {
            if (is_array($persons)) {
                $this->persons = $persons;
            } else {
                $this->persons = preg_split("/ ?\/ ?/", $persons);
            }
        } else {
            $this->persons = false;
        }
    }

    function as_array ()
    {
        return $this->persons;
    }
    
    function as_flag_array ()
    {
        $flag_persons = array();
        foreach ($this->persons as $person ){
            $flag_persons[] = self::create_flag($person);
        }
        return $flag_persons;
    }
    
    function as_string ()
    {
        return implode(" / ", $this->persons);
    }
}