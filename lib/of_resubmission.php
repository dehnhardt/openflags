<?php

class of_resubmission
{

    private $date = false;

    private $prefix = "RSM:";

    public function __construct ($resubmission = null)
    {
    	if( $resubmission ){
    	    $this->parse_date($resubmission);
    	}
    }

    public function set_date ($date)
    {
        $this->date = $date;
    }

    public function parse_date ($resubmission)
    {
        if (preg_match("/^$this->prefix.*/", $resubmission)) {
            $resubmission = substr($resubmission, 4);
        }
        if ($pattern = $this->is_date($resubmission)) {
            $this->date = DateTime::createFromFormat($pattern, $resubmission);
        } else {
            $this->date = false;
        }
    }

    public function get_date ()
    {
        return $this->date;
    }

    public function as_flag ()
    {
        return $this->date ? $this->prefix . $this->date->format('Y-m-d') : false;
    }

    public function is_valid ()
    {
        return $this->date ? true : false;
    }

    private function is_date ($date)
    {
        if (preg_match('/^[0-9]{4}\-(0[1-9]|1[0-2])\-([012][0-9]|3[01])$/', 
                $date)) {
            return 'Y-m-d';
        } else 
            if (preg_match('/^([012][0-9]|3[01])\.(0[1-9]|1[0-2])\.[0-9]{4}$/', 
                    $date)) {
                return 'd.m.Y';
            } else {
                return false;
            }
    }
}