<?php

class of_head_processor
{

    private $rcmail;

    private $openflags;

    private $name;

    private $message_priv_labels;

    private $infos;

    private $persons;

    public function __construct ($rcmail, $openflags)
    {
        $this->rcmail = $rcmail;
        $this->openflags = $openflags;
        $this->name = $openflags->name;
        $this->message_priv_labels = array();
    }

    public function message_load ($args)
    {
        $this->read_single_flags($args);
        // -- no return value for this hook
    }

    private function read_single_flags ($args)
    {
        // rcube::write_log('openflags', "start read_single_flags");
        $this->message_priv_labels = array();
        if ($this->openflags->of_parts['rsm']) {
            $this->message_priv_labels['rsm'][] = '';
        }
        if ($this->openflags->of_parts['info']) {
            $this->message_priv_labels['info'] = '';
        }
        if ($this->openflags->of_parts['person']) {
            $this->message_priv_labels['person'] = '';
        }
        if (is_array($args['object']->headers->flags)) {
            foreach ($args['object']->headers->flags as $flagname => $flagvalue) {
                $flag = $flagname;
                if (preg_match(
                        '/^\$?label|SEEN|DELETED|FLAGGED|RECENT|ANSWERED/', 
                        $flag)) {} else 
                    if (preg_match('/^\@.*/', $flag)) {
                        if (! $this->openflags->of_parts['person']) {
                            continue;
                        }
                        $this->message_priv_labels['person'][] = str_replace(
                                '_', ' ', substr($flag, 1));
                    } else 
                        if (preg_match('/^RSM:/', $flag)) {
                            if (! $this->openflags->of_parts['rsm']) {
                                continue;
                            }
                            $rsm = substr($flag, 4);
                            $date = DateTime::createFromFormat('Y-m-d', $rsm);
                            ;
                            $this->message_priv_labels['rsm'][0] = $date->format(
                                    'd.m.Y');
                        } else 
                            if ($this->openflags->of_parts['info']) {
                                $this->message_priv_labels['info'][] = $flag;
                            }
            }
        }
        // -- no return value for this hook
    }

    public function message_headers_output ($p)
    {
        rcube::write_log($this->name, "message_headers_output");
        $ret = $p['output'];
        $flags = array();
        $flags['title'] = 'Flags';
        $flags['value'] = $this->create_header_html();
        $flags['html'] = 1;
        $ret['flags'] = $flags;
        $p['output'] = $ret;
        return $p;
    }

    private function create_header_html ()
    {
        $html = '<table id="openflags"><tbody>';
        foreach ($this->message_priv_labels as $l => $k) {
            $val = '';
            $n = 0;
            if (is_array($k)) {
                foreach ($k as $d) {
                    if ($n > 0)
                        $val .= ' / ';
                    $val .= $d;
                    $n ++;
                }
            }
            $html .= '<tr><td class="first">' . $this->openflags->gettext($l) .
                     ':</td><td>' . $val;
            $html .= '<div class="openflags_dialog" id="openflags_dialog' . $l .
                     '" title = "' . $this->openflags->gettext($l) . '"></div>';
            $html .= '</td><td>';
            $html .= '<img class="openflags_edit" src="' .
                     $this->openflags->get_urlbase() .
                     'media/Edit.png" width="16px" height="16px" ' .
                     ' onclick="edit_flag(\'openflags_dialog' . $l . '\', \'' .
                     $l . '\', \'' . $val . '\')" ' . ' alt="' .
                     $this->openflags->gettext('edit') . '"/>';
            $html .= '</td></tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }

    function get_flag_data ()
    {
        $type = rcube_utils::get_input_value('type', rcube_utils::INPUT_POST);
        $div_id = rcube_utils::get_input_value('div_id', 
                rcube_utils::INPUT_POST);
        $value = rcube_utils::get_input_value('value', rcube_utils::INPUT_POST);
        $update = rcube_utils::get_input_value('update', 
                rcube_utils::INPUT_POST);
        rcube::write_log($this->name, "Type: $type, Update: $update");
        switch ($type) {
            case 'rsm':
                $new_value = $value;
                break;
            case 'info':
                $new_value = $this->openflags->get_infos();
                $current = preg_split('/ ?\/ ?/', $value);
                foreach ($new_value as $name => $val) {
                    if (in_array($name, $current)) {
                        $new_value[$name] = 1;
                    }
                }
                break;
            case 'person':
                $current = preg_split('/ ?\/ ?/', $value);
                $new_value = $this->openflags->get_persons();
                foreach ($new_value as $name => $val) {
                    if (in_array($name, $current)) {
                        $new_value[$name] = 1;
                    }
                }
                break;
        }
        $this->rcmail->output->command('plugin.of_show_edit_dialog', 
                array(
                        'type' => $type,
                        'value' => $new_value,
                        'div_id' => $div_id,
                        'update' => $update
                ));
    }

    function message_compose ($param)
    {
        rcube::write_log($this->name, "Compose-Params: " . print_r($param, true));
        return $param;
    }

    function update ()
    {
        $type = rcube_utils::get_input_value('type', rcube_utils::INPUT_POST);
        
        $uid = rcube_utils::get_input_value('_uid', rcube_utils::INPUT_POST);
        $mbox = rcube_utils::get_input_value('_mbox', rcube_utils::INPUT_POST);
        
        $formdata = rcube_utils::get_input_value('form_data', 
                rcube_utils::INPUT_POST);
        $storage = $this->rcmail->get_storage();
        rcube::write_log($this->name, "Type: " . $type);
        rcube::write_log($this->name, "All sent data: \n" . print_r($formdata, true));
        switch ($type) {
            case 'rsm':
                $rsm_mbox = $this->rcmail->config->get(
                        $this->openflags->rsm_folder_name);
                rcube::write_log($this->name, 'rsm_box: ' . $rsm_mbox);
                if (! rsm_mbox || $rsm_mbox == '') {
                    $this->rcmail->output->show_message(
                            'openflags.rsmfolder_missing', 'error', null, null, 
                            20);
                    break;
                }
                require_once 'of_resubmission.php';
                $rsm_date_old = new of_resubmission(
                        trim($formdata['rsm_date_old']));
                $rsm_date_new = new of_resubmission(
                        trim($formdata['rsm_date_new']));
                
                if ($rsm_date_old->is_valid()) {
                    $rsm_fdate_old = $rsm_date_old->as_flag();
                    if (! $storage->unset_flag($uid, $rsm_fdate_old, $mbox)) {
                        rcube::write_log($this->name, 
                                "Delete of $rsm_fdate_old failed");
                    }
                }
                if ($rsm_date_new->is_valid()) {
                    $rsm_fdate_new = $rsm_date_new->as_flag();
                    if (! $storage->set_flag($uid, $rsm_fdate_new, $mbox)) {
                        rcube::write_log($this->name, "Add of $rsm_fdate_new failed");
                    } else 
                        if ($mbox != $rsm_mbox) {
                            rcube::write_log($this->name, "Move message to $rsm_mbox");
                            if ($storage->move_message($uid, $rsm_mbox)) {
                                $this->rcmail->output->show_message(
                                        'openflags.rsm_saved', 'conformation', 
                                        array(
                                                'folder' => $rsm_mbox
                                        ));
                                sleep(5);
                            }
                        }
                }
                $this->rcmail->output->redirect(
                        array(
                                '_mbox' => $storage->get_folder()
                        ), 0);
                break;
            case 'info':
                rcube::write_log($this->name, 'formdata: ' . print_r($formdata, true));
                foreach ($formdata as $name => $value) {
                    if ($value == 'true') {
                        if (! $storage->set_flag($uid, $name, $mbox)) {
                            rcube::write_log($this->name, "Add of $name failed");
                        }
                    } else {
                        if (! $storage->unset_flag($uid, $name, $mbox)) {
                            rcube::write_log($this->name, "Delete of $name failed");
                        }
                    }
                }
                $this->rcmail->output->redirect(
                        array(
                                '_mbox' => $storage->get_folder()
                        ), 0);
                break;
            case 'person':
                require_once 'of_person.php';
                rcube::write_log($this->name, 'formdata: ' . print_r($formdata, true));
                foreach ($formdata as $name => $value) {
                    if ($value == 'true') {
                        if (! $storage->set_flag($uid, 
                                of_person::create_flag($name), $mbox)) {
                            rcube::write_log($this->name, "Add of $name failed");
                        }
                    } else {
                        if (! $storage->unset_flag($uid, 
                                of_person::create_flag($name), $mbox)) {
                            rcube::write_log($this->name, "Delete of $name failed");
                        }
                    }
                }
                $this->rcmail->output->redirect(
                        array(
                                '_mbox' => $storage->get_folder()
                        ), 0);
                break;
        }
    }
}
