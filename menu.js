/**
 * 
 */

function Contextmenu(P) {
	P = P || {};

	var PARAM = $.extend({
		trigger : {},
		list : "#clist",
		position : "left",
		keep : false,
		person : false,
		mboxprefix : 'Public/Artists/'
	}, P);

	var that = this;

	var el = PARAM.trigger instanceof jQuery ? PARAM.trigger : $(PARAM.trigger);
	var e_menu = PARAM.list instanceof jQuery ? PARAM.list : $(PARAM.list);

	el.mouseenter(function(e) {
		that.visible(e, true);
	});
	el.mouseleave(function(e) {
		that.visible(e, false);
	});
	e_menu.mouseleave(function(e) {
		that.visible(false);
	});

	// return {

	this.init = function() {
		if (PARAM.person) {
			e_menu.children().each(function(index) {
				artist = $(this).text();
				path = PARAM.mboxprefix + artist;
				mb = $("li.mailbox a[rel='" + path + "']");
				console.log('MB-Path neu - an enable_submenu: ' + path );
				/*if (mb.length > 0) {
					$(this).data('path', path);
					that.enable_submenu($(this), mb, path);
				}*/
			});
		}
	};

	this.enable_submenu = function(listelement, mb, mbpath) {
		PARAM.keep = true;
		listelement.addClass("has_children");
		/*listelement.click(data={ le: listelement, mailbox:mb, path:mbpath }, function(event) {
			event.stopPropagation();
			that.show_submenu(data.le, data.mailbox, data.path );
		});*/
		listelement.click( data={mailbox: mb, path: mbpath }, function(event) {
			//console.log( 'data.path: ' + data.path + ' or mbpath: ' +mbpath );
			event.stopPropagation();			
			that.show_submenu($(event.target), mb, mbpath );
		});
	};

	this.show_submenu = function(listelement, mb, mbpath) {
		//console.log( mbpath);
		var cl = listelement.children("ul");
		if (cl.length == 0) {
			var ul = $('<ul class="copy_move_list">');
			$("<li class='action'>").text("copymail").appendTo(ul);
			$("<li class='action'>").text("movemail").appendTo(ul);
			if (mb.parent().length > 0) {
				children = mb.parent().children("ul").children("li").children( "a");
				if (children.length > 0 ) {
					children.each(function(index) {
						name = $(this).text();
						li = $("<li>").text( name );
						path=mbpath+'/'+name;
						mb = $("li.mailbox a[rel='" + path + "']");
						li.appendTo(ul);
						li.data('path', path);
						console.log('MB-Path neu - an enable_submenu: ' + path );
						that.enable_submenu($(this), mb, path);
					});
				}
			}
			ul.appendTo(listelement);
		}
	};

	this.visible = function(event, visible) {
		if (visible) {
			x = el.offset().left + el.width();
			y = el.offset().top;
			e_menu.css('display', 'block');
			e_menu.offset({
				left : x,
				top : y
			});
		} else {
			if (!(PARAM.keep && this.event_in(event, e_menu))) {
				e_menu.css('display', 'none');
			}
		}
	};

	this.event_in = function(mouseevent, element) {
		var left = mouseevent.pageX;
		var top = mouseevent.pageY;
		var offset = element.offset();
		var element_left = offset.left;
		var element_top = offset.top;
		var element_right = element_left + element.width();
		var element_bottom = element_top + e_menu.height();

		return (left >= element_left && left <= element_right
				&& top >= element_top && top <= element_bottom);
	};
}
