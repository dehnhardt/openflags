<?php

/**
 * OpenFlags
 *
 * Plugin to show and edit custom message flags
 *
 * @version 1.0
 * @author Holger Dehnhardt
 * @url http://roundcube.net/plugins/openflags
 */
class openflags extends rcube_plugin
{

    public $task = 'mail|settings|login';

    public $rsm_folder_name = 'of_rsm_mbox';

    public $rsm_last_update = 'of_rsm_last_update';

    public $system_flags = false;

    private $of_head_processor;

    private $of_list_processor;

    private $of_settings_processor;

    private $of_compose_processor;

    private $of_search_processor;

    public $of_parts;
    
    private $infos;
    
    private $persons;
    

    function init ()
    {
        require_once 'lib/of_head_processor.php';
        require_once 'lib/of_list_processor.php';
        require_once 'lib/of_settings_processor.php';
        require_once 'lib/of_compose_processor.php';
        require_once 'lib/of_search_processor.php';
        
        $this->name = get_class($this);
        $this->rcmail = rcmail::get_instance();
        
        $this->include_script('openflags.js');
        $this->rcmail->load_language(null, array(
                'openflags' => ' ',
                'xsearch'=> 'erw. Suche',
                'xsearchtitle'=> 'Ausführliche Suchmaske'
        ));
        $this->add_texts('localization', true);
        
        $this->load_config();
        $this->of_parts = $this->rcmail->config->get('of_parts', array());
        
        $dont_override = $this->rcmail->config->get('dont_override', array());
        
        rcube::write_log($this->name, 
                'init: task= ' . $this->rcmail->task . ' / action= ' .
                         $this->rcmail->action);
        
        if ($this->rcmail->task == 'login') {
            $this->add_hook('login_after', 
                    array(
                            $this,
                            'login_after'
                    ));
        } else 
            if ($this->rcmail->task == 'mail') {
                $this->include_stylesheet(
                        $this->local_skin_path() . '/openflags.css');
                
                $this->of_head_processor = new of_head_processor($this->rcmail, 
                        $this);
                $this->of_search_processor = new of_search_processor(
                        $this->rcmail, $this);
                
                $this->add_hook('startup', array($this->of_search_processor, 'redirect'));
                
                $this->register_action('plugin.openflags-search', 
                        array(
                                $this->of_search_processor,
                                'search'
                        ));
                $this->register_action('plugin.openflags-get_searchformdata',
                        array(
                                $this->of_search_processor,
                                'get_searchformdata'
                        ));
                
                if ($this->rcmail->action == 'list' ||
                         $this->rcmail->action == 'refresh' ||
                         $this->rcmail->action == 'search' ||
                         $this->rcmail->action == 'plugin.openflags-search' ||
                         $this->rcmail->action == '') {
                    $this->of_list_processor = new of_list_processor(
                            $this->rcmail, $this);
                    
                    if (! in_array('list_cols', $dont_override)) {
                        rcube::write_log($this->name, "override allowed");
                        $this->add_hook('startup', 
                                array(
                                        $this->of_list_processor,
                                        'startup'
                                ));
                    }
                    $this->add_hook('messages_list', 
                            array(
                                    $this->of_list_processor,
                                    'messages_list'
                            ));
                }
                if ($this->rcmail->action == 'show' ||
                         $this->rcmail->action == 'preview') {
                    $this->add_hook('message_load', 
                            array(
                                    $this->of_head_processor,
                                    'message_load'
                            ));
                    $this->add_hook('message_headers_output', 
                            array(
                                    $this->of_head_processor,
                                    'message_headers_output'
                            ));
                } else 
                    if ($this->rcmail->action == '') {
                        $this->add_hook('message_load', 
                                array(
                                        $this->of_head_processor,
                                        'message_load'
                                ));
                    } else 
                        if ($this->rcmail->action == 'compose' ||
                                 $this->rcmail->action == 'send') {
                            $this->of_compose_processor = new of_compose_processor(
                                    $this->rcmail, $this);
                            $this->add_hook('render_page', 
                                    array(
                                            $this->of_compose_processor,
                                            'render_page'
                                    ));
                            $this->add_hook('message_outgoing_headers', 
                                    array(
                                            $this->of_compose_processor,
                                            'message_outgoing_headers'
                                    ));
                        }
                // register actions
                $this->register_action('plugin.openflags-get_flag_data', 
                        array(
                                $this->of_head_processor,
                                'get_flag_data'
                        ));
                $this->register_action('plugin.openflags-update', 
                        array(
                                $this->of_head_processor,
                                'update'
                        ));
            } elseif ($this->rcmail->task == 'settings') {
                $this->of_settings_processor = new of_settings_processor(
                        $this->rcmail, $this);
                if (! in_array('rsm_mbox', $dont_override)) {
                    $this->add_hook('preferences_list', 
                            array(
                                    $this->of_settings_processor,
                                    'preferences_list'
                            ));
                    $this->add_hook('preferences_save', 
                            array(
                                    $this->of_settings_processor,
                                    'preferences_save'
                            ));
                }
            }
        
        rcube::write_log($this->name, "exit INIT");
    }

    public function login_after ($param)
    {
        require_once 'lib/of_resubmission.php';
        rcube::write_log($this->name, 'Login');
        $mbox = $this->rcmail->config->get($this->rsm_folder_name);
        if ($mbox) {
            $rsm_date_old = DateTime::createFromFormat('Y-m-d', 
                    $this->rcmail->config->get('of_rsm_last_update') ? $this->rcmail->config->get(
                            'of_rsm_last_update') : '2000-01-01');
            $rsm_date_new = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
            if ($rsm_date_old < $rsm_date_new) {
                $storage = $this->rcmail->get_storage();
                $rsm_message_ids = $storage->search_once($mbox, 'ALL SEEN')->get();
                foreach ($rsm_message_ids as $rsm_message_id) {
                    $rsm_message = $storage->get_message($rsm_message_id, $mbox);
                    $flags = $rsm_message->flags;
                    foreach ($flags as $flag => $set) {
                        $resubmission = new of_resubmission($flag);
                        if ($resubmission->is_valid()) {
                            if ($resubmission->get_date() <= $rsm_date_new) {
                                $storage->unset_flag($rsm_message->uid, 'seen', 
                                        $mbox);
                            }
                        }
                    }
                }
                $config = array();
                $config['of_rsm_last_update'] = $rsm_date_new->format('Y-m-d');
                $user = new rcube_user($this->rcmail->get_user_id());
                $user->save_prefs($config);
            }
        }
        return $param;
    }
    
    function get_flags ($types)
    {
        $flags = array();
        if (!is_array($types)) {
            $types = preg_split('/ /', $types);
        }
        foreach ($types as $type) {
            switch ($type) {
            	case 'info':
            	    $flags[$type] = $this->get_infos();
            	    break;
            	case 'person':
            	    $flags[$type] = $this->get_persons();
            	    break;
            }
        }
        return $flags;
    }
    
    function get_persons ()
    {
        if (! $this->persons) {
            $db = rcube::get_instance()->get_dbh();
            $sql_result = $db->query(
                    "SELECT upper(artistname), 0 FROM artist order by artistname");
            $persons = array();
            while ($sql_result && ($sql_arr = $db->fetch_array($sql_result))) {
                $persons[$sql_arr[0]] = $sql_arr[1];
            }
        }
        return $persons;
    }
    
    function get_infos ()
    {
        if (! $this->infos) {
            $this->infos = $this->rcmail->config->get('of_info_flags');
        }
        return $this->infos;
    }

    public function get_urlbase ()
    {
        return $this->urlbase;
    }
}

