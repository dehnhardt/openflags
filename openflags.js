var form_array = {};
form_array['person'] = {};
form_array['info'] = {};

var old_sent_box;

rcmail.addEventListener('listupdate',openflags_add_contextmenu );

rcmail.addEventListener('init', function(evt) {
	if (window.rcmail) {
		rcmail.addEventListener('plugin.openflags-show_search_dialog',
				openflags_show_search_dialog);

		if (rcmail.message_list ) {
			rcmail.message_list.addEventListener('select',
					openflags_xsearch_msglist_select);
		}

		rcmail.register_command('plugin.openflags-get_searchformdata',
				openflags_getsearchformdata, true);

		$("a.xsearch").insertAfter( $("#messagemenulink") );
	}
});

function openflags_add_contextmenu(){
	$(".of_show_details").each(
			function( index ){
				var id = $(this).attr("id");
				var nr = id.substr(id.indexOf('_'));
				if( nr.indexOf( 'person')==0){
					
				}
				var c = new Contextmenu({
					trigger: $(this),
					list: '#menu' + nr,
					person: nr.indexOf( 'person')>=0
				}); 	
				c.init();
			}
	);
	
}

function openflags_xsearch_msglist_select(list) {
	if ( rcmail.env.all_folder_search_uid_mboxes) {
		console.log('message: ' + list.selection);
		if (list.selection.length == 1) {
			var uid = list.selection[0];
			var tr = rcmail.env.all_folder_search_uid_mboxes[uid];
			var mbox = tr.mbox;
			if (rcmail.env.mailbox != mbox) {
				rcmail.select_folder(mbox, '', true);
				rcmail.env.mailbox = mbox;
			}
		}
		rcmail.message_list.draggable = false;
	} else {
		rcmail.message_list.draggable = true;

	}
}

function openflags_getsearchformdata() {
	if (window.rcmail && rcmail.env.task == 'mail') {
		data = rcmail.selection_post_data();
		rcmail.http_post('plugin.openflags-get_searchformdata', data);
	}
}

function openflags_show_search_dialog(response) {

	var of_search_dialog = $('#of_search_dialog');

	if ((typeof of_search_dialog[0]) == 'undefined') {
		of_search_dialog = jQuery('<div/>', {
			id : 'of_search_dialog',
			title : rcmail.gettext('xsearch', 'openflags')
		});
		var form = jQuery('<form/>', {
			id : 'of_search_form'
		});
		form.appendTo(of_search_dialog);
		var p = jQuery('<p/>');
		$('<label>').attr('for', '_xs_q').html('Suchwert').appendTo(p);
		var searchtext = jQuery('<input>', {
			id : '_xs_q',
			name : '_xs_q',
			type : 'text'
		});
		searchtext.attr('size', 50);
		searchtext.appendTo(p);
		$("<br/>").appendTo(p);
		var rbAnd = jQuery('<input>', {
			id : 'xs_q_type_and',
			value : 'and',
			checked : true,
			name : '_xs_q_type',
			type : 'radio',
		});
		rbAnd.appendTo(p);
		$("<label>").attr('for', 'xs_q_type_and').text(
				rcmail.gettext('query_type_and', 'openflags')).appendTo(p);
		var rbOr = jQuery('<input>', {
			id : 'xs_q_type_or',
			value : 'or',
			name : '_xs_q_type',
			type : 'radio',
		});
		rbOr.appendTo(p);
		$("<label>").attr('for', 'xs_q_type_or').text(
				rcmail.gettext('query_type_or', 'openflags')).appendTo(p);

		p.appendTo(form);
		var fs = jQuery('<fieldset>');
		$('<legend>').attr('id', 'fs_scope').text(
				rcmail.gettext('folder_scope', 'openflags') + ' ('
						+ rcmail.env.mailbox + ')').appendTo(fs);
		p = jQuery('<p/>');
		p.appendTo(fs);
		var rbsingle = jQuery('<input>', {
			id : 'xs_single',
			value : 'single',
			name : '_xs_scope',
			type : 'radio',
			checked : true
		});
		rbsingle.appendTo(p);
		$("<label>").attr('for', 'xs_single').text(
				rcmail.gettext('folder_scope_single', 'openflags')).appendTo(p);
		var rbrecurse = jQuery('<input>', {
			id : 'xs_recurse',
			value : 'recurse',
			name : '_xs_scope',
			type : 'radio'
		});
		rbrecurse.appendTo(p);
		$("<label>").attr('for', 'xs_recurse').text(
				rcmail.gettext('folder_scope_recurse', 'openflags'))
				.appendTo(p);
		var rball = jQuery('<input>', {
			id : 'xs_all',
			value : 'all',
			name : '_xs_scope',
			type : 'radio'
		});
		rball.appendTo(p);
		$("<label>").attr('for', 'xs_all').attr('id', 'lbl_path').text(
				rcmail.gettext('folder_scope_all', 'openflags')).appendTo(p);

		fs.appendTo(form);

		sp1 = create_checkbutton(response.value.info, 'info', false);
		sp1.appendTo(form);
		sp2 = create_checkbutton(response.value.person, 'person', false);
		sp2.appendTo(form);
		stext = rcmail.gettext('somelabel', 'plugin-name');
		of_search_dialog.dialog({
			width : '575px',
			modal: true,
			'min-height' : '480px',
			buttons : {
				"Suche" : function() {
					of_post_search();
					//$(this).dialog("close");
				},
				"Abbruch" : function() {
					$(this).dialog("close");
				}
			}
		});
		of_search_dialog.keypress(function(e) {
		    if (e.keyCode == $.ui.keyCode.ENTER) {
		        e.preventDefault();
		        of_post_search();
		    }
		});
	} else {
		$('#fs_scope').text(
				rcmail.gettext('folder_scope', 'openflags') + ' ('
				+ rcmail.env.mailbox + ')');
		of_search_dialog.dialog("open");
	}
    /*form = dialog.find( "#of_search_dialog" ).on( "submit", function( event ) {
        event.preventDefault();
        of_post_search();
        dialog.close();
      });*/

}

function of_post_search() {
	var r, lock = rcmail.set_busy(true, 'searching');

	rcmail.clear_message_list();

	url = search_params();

	if (rcmail.env.source)
		url._source = this.env.source;
	if (rcmail.env.group)
		url._gid = this.env.group;

	// reset vars
	rcmail.env.current_page = 1;

	var action = 'plugin.openflags-search';

	r = rcmail.http_request(action, url, lock);

	rcmail.env.qsearch = {
		lock : lock,
		request : r
	};
	$("#of_search_dialog").dialog("close");
	//rcmail.set_busy(false, 'searching');
}

function create_checkbutton(values, name, with_handler) {
	with_handler = ((typeof with_handler) == 'undefined') ? true : with_handler;
	var sp = jQuery('<div/>', {
		id : 'section-scroll',
		'class' : 'of_scrollable'
	});
	$.each(values, function(a, b) {
		p = jQuery('<p class="narrow"/>');
		i = jQuery('<input/>', {
			type : 'checkbox',
			name : name,
			value : a,
			checked : b == 0 ? false : true
		});
		if (with_handler) {
			i.bind('change', function() {
				of_toggle_info(name, $(this));
			});
		}
		s = jQuery('<span>', {
			text : a
		});
		i.appendTo(p);
		s.appendTo(p);
		p.appendTo(sp);
	});
	return sp;
}

/* function openflags_search() {
	var value = rcmail.gui_objects.qsearchbox.value;
	if (value) {
		var r, lock = rcmail.set_busy(true, 'searching'), url = rcmail
				.search_params(value);

		rcmail.clear_message_list();

		if (rcmail.env.source)
			url._source = this.env.source;
		if (rcmail.env.group)
			url._gid = this.env.group;

		// reset vars
		rcmail.env.current_page = 1;

		var action = 'plugin.openflags-search';

		r = rcmail.http_request(action, url, lock);

		rcmail.env.qsearch = {
			lock : lock,
			request : r
		};

	}

}*/

function search_params() {
	var n;
	var url = {};
	var mods_arr = [];
	var filter;
	var mods = rcmail.env.search_mods;
	var mbox = rcmail.env.mailbox;

	filter = rcmail.gui_objects.search_filter.value;

	var form = $('#of_search_form');

	key_val = form.serializeArray();
	$.each(key_val, function(a, b) {
		value = b.value;
		if (!url[b.name]) {
			url[b.name] = b.value;
		} else {
			if (!$.isArray(url[b.name])) {
				v = url[b.name];
				url[b.name] = [];
				url[b.name].push(v);
			}
			url[b.name].push(value);
		}
	});
	if (filter) {
		url._filter = filter;
	}
	if (mods && this.message_list)
		mods = mods[mbox] ? mods[mbox] : mods['*'];

	if (mods) {
		for (n in mods) {
			mods_arr.push(n);
		}
		url._headers = mods_arr.join(',');
	}

	if (mbox)
		url._mbox = mbox;

	return url;
};

function openflags_visible(element, parent, visible) {
	if (visible) {

		x = $(parent).offset().left + $(parent).width();
		y = $(parent).offset().top;
		$(element).css('display', 'block');
		$(element).offset({
			left : x,
			top : y
		});
	} else {
		$(element).css('display', 'none');
	}
}

function edit_flag(div_id, type, value) {
	rcmail.addEventListener('plugin.of_show_edit_dialog', of_show_edit_dialog);
	if (window.rcmail && rcmail.env.task == 'mail') {
		data = rcmail.selection_post_data({
			_flag : type
		});
		data['div_id'] = div_id;
		data['type'] = type;
		data['value'] = value;
		data['update'] = 'server';
		rcmail.http_post('plugin.openflags-get_flag_data', data);
	}
}

function compose_flag(div_id, type, value) {
	rcmail.addEventListener('plugin.of_show_edit_dialog', of_show_edit_dialog);
	value = $('#openflags_edit' + type).val();
	if (window.rcmail && rcmail.env.task == 'mail') {
		var data = {};
		data['div_id'] = div_id;
		data['type'] = type;
		data['value'] = value;
		data['update'] = 'client';
		rcmail.http_post('plugin.openflags-get_flag_data', data);
	}
}

function of_show_edit_dialog(response) {
	var type = response.type;
	var val = response.value;
	var update = response.update;

	var dialog = $("#" + response.div_id);
	var inner_div_id = "inner_" + response.div_id;
	var div = $('#' + inner_div_id);
	if (div) {
		div.remove();
	}
	div = jQuery('<form/>', {
		id : inner_div_id
	});
	input = jQuery('<input/>', {
		type : 'hidden',
		id : 'of_type_id',
		name : 'type',
		value : type
	});
	input.appendTo(div);

	switch (type) {
	case 'rsm':
		var span = jQuery('<span>');// document.createElement('span');
		var txt = document.createTextNode('Datum zur Wiedervorlage: ');
		var input_rsm_alt = jQuery('<input/>', {
			type : 'hidden',
			name : 'rsm_date_old',
			value : val
		});
		var datepicker = jQuery('<input/>', {
			type : 'text',
			id : 'datepicker',
			name : 'rsm_date_new',
			value : val
		});
		datepicker.blur();
		datepicker.datepicker().datepicker("hide");
		input_rsm_alt.appendTo(div);
		span.append(txt);
		span.appendTo(div);
		datepicker.appendTo(div);
		break;
	case 'info':
	case 'person':
		sp = jQuery('<div/>', {
			id : 'section-scroll',
			'class' : 'of_scrollable'
		});
		sp = create_checkbutton(val, type);
		sp.appendTo(div);
		break;
	}

	dialog.append(div);
	dialog.dialog({
		buttons : {
			"Ok" : function() {
				if (update == 'client') {
					of_set_values($(this).attr("id"), type);
				} else {
					of_post_update($(this).attr("id"), type);

				}
				$(this).dialog("close");
			},
			"Abbruch" : function() {
				$(this).dialog("close");
			}
		}
	});
}

function of_toggle_info(type, element) {
	var name = element.attr('value');
	var add = element.is(':checked');
	if (form_array[type][name]) {
		delete form_array[type][name];
	} else {
		form_array[type][name] = add;
	}
}

function of_get_array_as_string(array) {
	var value = '';
	var n = 0;
	for ( var i in array) {
		var el = array[i];
		if (el) {
			if (n > 0) {
				value += ' / ';
			}
			value += i;
			n++;
		}
	}
	return value;
}

function get_formadata(dialogid, type, extern) {
	var formdata = {};
	switch (type) {
	case 'rsm':
		var $elements = $("input[name^='rsm_date']");
		for (var i = 0; i < $elements.length; i = i + 1) {
			var e = $($elements[i]);
			var name = e.attr('name');
			var value = e.val();
			formdata[name] = value;
		}
		break;
	case 'info':
	case 'person':
		if (extern) {
			formdata = form_array[type];
		} else {
			formdata = of_get_array_as_string(form_array[type]);
		}
		break;
	}
	return formdata;
}

function of_set_values(dialogid, type) {
	// var type = $('#of_type_id').attr('value');
	var formdata = get_formadata(dialogid, type, false);
	var edit = $('#openflags_edit' + type);
	var value = '';
	switch (type) {
	case 'rsm':
		value = formdata['rsm_date_new'];
		if( value ){
			old_sent_box = $("select[name='_store_target'] option[selected='selected']");
			var rsmboxname=rcmail.env.of_rsm_mbox;
			var option = $("select[name='_store_target'] option[value='"+ rsmboxname+"']");
			option.attr('selected','true');
		}else{
			if(old_sent_box){
				old_sent_box.attr('selected','true');				
			}
		}
		edit.val(value);
		break;
	case 'info':
	case 'person':
		value = formdata;
		edit.val(value);
		break;
	}

}

function of_post_update(dialogid, type) {
	// var type = $('#of_type_id').attr('value');
	var formdata = get_formadata(dialogid, type, true);
	if (window.rcmail && rcmail.env.task == 'mail') {
		data = rcmail.selection_post_data({
			"type" : type,
			form_data : formdata
		});
		rcmail.http_post('plugin.openflags-update', data);
	}
}
