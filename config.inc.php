<?php

$rcmail_config['of_info_flags'] = array (
		"TERMIN" => 0,
        "NEUER_TERMIN" => 0,
		"HONORAR" => 0,
		"PROGRAMM" => 0,
		"ABSAGE" => 0,
		"ANGEBOT" => 0
);

$rcmail_config['of_parts'] = array( 
	'rsm' => true,
	'info'=> true,
	'person' => true
);

$rcmail_config['of_person_name'] = 'Künstler';
