<?php
$labels = array();

// login page
$labels['person']  = 'Artist';
$labels['[flags]']  = 'Flags';
$labels['info']   = 'Information';
$labels['openflags']  = 'Flags';
$labels['rsm']   = 'Resubmission';
$labels['xsearch']   = 'Extended Search';

$messages['rsmfolder_missing'] = 'You need to configure a mailbox for resubmissions in the spcial folder section of the settings.';
$messages['rsm_saved'] = 'We have filed as resubmission for this message and moved it to $folder.';
$messages['ofs_too_much_results'] = 'Too much messages found. Showing the first 100 resuts. Please refine your search.';