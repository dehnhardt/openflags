<?php
//$labels = array();

// login page
$labels['flags']  = 'Merkmale';
$labels['info']   = 'Information';
$labels['openflags'] = 'Mekmale';
$labels['person']  = 'Künstler';
$labels['query_type_and'] = 'Alle Wörter suchen';
$labels['query_type_or'] = 'Mindestens ein Wort suchen';
$labels['rsm']   = 'Wiedervorlage';
$labels['folder_scope']   = 'Suchtiefe';
$labels['folder_scope_single']   = 'Dieser Ordner';
$labels['folder_scope_recurse']   = 'Untergeordnete Ordner';
$labels['folder_scope_all']   = 'Alle Ordner';
$labels['xsearch']   = 'Erweiterte Suche';
$labels['xsearchtitle'] = "Dialog für erweiterte Suche aufrufen";

$messages['rsmfolder_missing'] = 'Für die Wiedervorlage ist kein Ordner zugeornet.<br/> Bitte in Einstellungen->Spezailordner zuweisen.';
$messages['rsm_saved'] = 'Für die Nachricht ist eine Wiedervorlage angelegt. Die Nachricht wurde in den Ordner $folder verschoben.';
$messages['ofs_too_much_results'] = 'Es wurden zu viele Nachrichten gefunden, daher werden nur die ersten 100 angezeigt. Bitte die Suche verfeinern.';
